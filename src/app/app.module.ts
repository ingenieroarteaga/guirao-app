import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ClientesPage } from '../pages/clientes/clientes';
import { ProductosPage } from '../pages/productos/productos';
import { PedidosPage } from '../pages/pedidos/pedidos';
import { AjustesPage } from '../pages/ajustes/ajustes';
import { ResumenPage } from '../pages/resumen/resumen';
import { PedidosModalClientePage } from '../pages/pedidos-modal-cliente/pedidos-modal-cliente';
import { PedidosModalProductosPage } from '../pages/pedidos-modal-productos/pedidos-modal-productos';
import { AjustesModalUsuarioPage } from '../pages/ajustes-modal-usuario/ajustes-modal-usuario';
import { Network } from '@ionic-native/network';

import {HttpClientModule} from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ClientesProvider } from '../providers/clientes/clientes';
import { AuthProvider } from '../providers/auth/auth';
import { TokenProvider } from '../providers/token/token';

//para guardar en storaga
import { IonicStorageModule } from '@ionic/storage/esm5';
import { PedidosProvider } from '../providers/pedidos/pedidos';
import { StockProvider } from '../providers/stock/stock';
import { ResumenProvider } from '../providers/resumen/resumen';
import { MarcasProvider } from '../providers/marcas/marcas';
import { FamiliaProductosProvider } from '../providers/familia-productos/familia-productos';

// importo el acordeon de MATERIAL
//import {MatExpansionModule} from '@angular/material/expansion';

// import { NetworkStatusProvider } from '../providers/network-status/network-status';

// import { ClientesPage, ProductosPage} from "../pages/index.page";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ClientesPage,
    ProductosPage,
    PedidosPage,
    AjustesPage,
    ResumenPage,
    PedidosModalClientePage,
    PedidosModalProductosPage,
    AjustesModalUsuarioPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HttpModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ClientesPage,
    ProductosPage,
    PedidosPage,
    AjustesPage,
    ResumenPage,
    PedidosModalClientePage,
    PedidosModalProductosPage,
    AjustesModalUsuarioPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ClientesProvider,
    AuthProvider,
    TokenProvider,
    Network,
    PedidosProvider,
    StockProvider,
    MarcasProvider,
    ResumenProvider,
    FamiliaProductosProvider
  ]
})
export class AppModule {}
