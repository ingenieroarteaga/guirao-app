import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GLOBAL } from "../../app/global";
import { TokenProvider } from "../token/token";
import { Productos } from '../../interfaces/productos.interface';

@Injectable()
export class MarcasProvider {

  path: string = GLOBAL.urlMarcas; // http://185.37.226.154:8080/guirao/marcas
  pathProducto: string = GLOBAL.urlProductos; // http://185.37.226.154:8080/guirao/producto
  loading: any;
  //creo variables para enviar cabecera con token
  authHeader: any;
  headersObj = new Headers();

  marcas: any =  [];

  productos: any = [];

  constructor(public http: HttpClient,
    private token: TokenProvider) {
    console.log('Marca Provider');
  }


  //utizo esta funcion para traer los datos del servidor de MARCAS
  getMarcas() {
    //obtengo el token
    this.authHeader = this.token.getToken();
    //asigno el token a la cabecera
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.authHeader
    });
    //realizo la peticíon
    return this.http.get(this.path, { headers });
  }

  getProductos() {
    //obtengo el token
    this.authHeader = this.token.getToken();
    //asigno el token a la cabecera
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.authHeader
    });
    //realizo la peticíon
    return this.http.get(this.pathProducto, {headers});
  }

  filtrarMarcas(marcas: any) {
    console.log('que ingresa a la funcion de marcas', marcas);
    console.log('que tiene los productos: ', this.getProductos());

    // marcas.push(marcas.id)
    
    return this.http.get<Productos>(this.productos.marcas.id = marcas.id);
  }
}
