import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GLOBAL } from "../../app/global";
import { Platform } from 'ionic-angular';
import { TokenProvider } from "../token/token";
import { Storage } from '@ionic/storage/esm5';


@Injectable()
export class ClientesProvider {

  path: string = GLOBAL.urlClientes;
  clientes: any = [];
  loading: any;
  //creo variables para enviar cabecera con token
  authHeader: any;
  headersObj = new Headers();


  constructor(public http: HttpClient, private platform: Platform,
    public storage: Storage, private token: TokenProvider) {
    console.log(' Clientes Provider ');
  }


  primeraCarga() {

    // si tengo el archivo guardado uso ese mientras descargo el nuevo
    // para pruebas en escirtorio usar localStorage.getItem
    // if (localStorage.getItem("clientes")) {
    this.storage.get("clientes").then((data) => {
      if (data != undefined) {

        console.log("uso lista clientes almacenada mientras cargo");
        this.cargarStorage();


        //obtengo los clientes
        console.log('ya cargue ahora pido lista')
        this.getClientes()
          .subscribe(data => {
            console.log(data);
            this.clientes = data;
            console.log("ya cargue la nueva lista de clientes");
            //envio la lista actualizada para almacenar
            this.guardarStorage();

          });

      } else {
        //obtengo los clientes
        console.log('no tengo backup guardado, pido la lista de clientes')
        this.getClientes()
          .subscribe(data => {
            console.log(data);
            this.clientes = data;
            //envio la lista actualizada para almacenar
            this.guardarStorage();

          });

      }
    })

  }

  //utizo esta funcion para traer los datos del servidor
  getClientes() {
    //obtengo el token
    this.authHeader = this.token.getToken();
    //asigno el token a la cabecera
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.authHeader
    });
    //realizo la peticíon
    return this.http.get(this.path, { headers });

  }

  // Sino tengo internet le indico a la app que use la
  // ultima lista de clientes que descargamos.
  cargarStorage() {
    let promesa = new Promise((resolve, reject) => {

      if (this.platform.is("cordova")) {

        console.log("inicializando storage clientes mobile")
        this.storage.ready()
          .then(() => {

            this.storage.get("clientes")
              .then(clientes => {
                this.clientes = clientes;
                console.log("uso clientes ya guardado mobile");
              })
          })

      } else {

        if (localStorage.getItem("clientes")) {

          this.clientes = JSON.parse(localStorage.getItem("clientes"));
          console.log("ya está cargado cliente almacenado");

        } else {

          this.primeraCarga();
          resolve();
        }

      }
    });
    return promesa;
  }


  // Guardo la lista de clientes actualizada.
  guardarStorage() {

    if (this.platform.is("cordova")) {

      this.storage.ready()
        .then(() => {

          this.storage.set("clientes", this.clientes);
          console.log("se guardo el archivo de clientes en mobile.");

        })

    } else {
      // este codigo sirve para guardar en local desktop
      localStorage.setItem("clientes", JSON.stringify(this.clientes));
      console.log("se guardo el archivo de clientes.");
    }

  }



  getClientesFiltro(searchTerm, clientesPor) {

    // set val to the value of the searchbar
    const val = searchTerm;

    //obtengo el filtro agregado por el usuario
    const buscarClientePor = clientesPor;

    //Se filtra o por campo dni o por apellido
    if (buscarClientePor == "nombre") {
      // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
        return this.clientes.filter((cliente) => {
          return (cliente.persona.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      } else {
        console.log("salida1");
        return this.clientes;
      }
    }
    else if (buscarClientePor == "apellido") {

      if (val && val.trim() != '') {
        return this.clientes.filter((cliente) => {
          return (cliente.persona.apellido !== null &&
            cliente.persona.apellido.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      } else {
        console.log("salida2");
        return this.clientes;
      }

    }
    else if (buscarClientePor == "cuit") {

      if (val && val.trim() != '') {
        return this.clientes.filter((cliente) => {
          return (cliente.persona.cuit.toString().indexOf(val.toString()) > -1);
        })
      } else {
        console.log("salida3");
        return this.clientes;
      }

    }
    else {
      console.log('Salida de error, no se encontro ningun filtro de clientePor!');
      return this.clientes;
    }
  }
}
