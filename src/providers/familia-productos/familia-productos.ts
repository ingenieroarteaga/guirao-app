import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GLOBAL } from "../../app/global";
import { TokenProvider } from '../token/token';

/*
  Generated class for the FamiliaProductosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FamiliaProductosProvider {

  path: string = GLOBAL.urlFamiliaProd; //http://185.37.226.154:8080/guirao/familias-productos
  loading: any;
  authHeader: any;
  headersObj = new Headers();

  constructor(public http: HttpClient,
    private token: TokenProvider) {
    console.log('Hello FamiliaProductosProvider Provider');
  }

  //utizo esta funcion para traer los datos del servidor
  getProductosFamilia() {
    //obtengo el token
    this.authHeader = this.token.getToken();
    //asigno el token a la cabecera
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.authHeader
    });
    //realizo la peticíon
    return this.http.get(this.path, { headers });
  }

}
