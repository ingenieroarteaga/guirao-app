import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GLOBAL } from "../../app/global";
import { LoadingController, Platform, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage/esm5';
import { TokenProvider } from "../token/token";

/*
  Generated class for the PedidosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StockProvider {
  stocks: any = [];
  path: string = GLOBAL.urlStocks;
  loading: any;
  //creo variables para enviar cabecera con token
  authHeader: any;
  headersObj = new Headers();


  constructor(public http: HttpClient,
    public loadingCtrl: LoadingController,
    private platform: Platform,
    private storage: Storage,
    private token: TokenProvider,
    public alertCtrl: AlertController) {

    console.log('Stocks provider');

  }

  // obtengo la lista de productos actualizada
  primeraCarga() {
    // Si ya existe un archivo guardado, trabajo con ese hasta que se termine de bajar el nuevo
    //uso this.storage para mobile y localStorage para Desktop
    // if (localStorage.getItem("stocks")) {
    this.storage.get("stocks").then((data) => {
      if (data != undefined) {
        // Creo el loading
        this.loading = this.loadingCtrl.create({
          content: 'Cargando datos locales...',
        });
        // muestro el loading
        this.loading.present();

        console.log("uso stock guardado mientras descargo");
        this.cargarStorage();
        this.loading.dismiss();

        console.log('ya cargue el guardado pero igual descargo el actuazalizado');
        this.getStocks()
          .subscribe(data => {
            this.stocks = data;
            console.log('ya guarde e iguale el actualizado');
            // envio notificación de actualizacióna
            const alert = this.alertCtrl.create({
              title: 'Productos actualizados',
              subTitle: 'La lista de productos ha sido actualizada',
              buttons: ['OK']
            });

            //envio la lista actualizada para almacenar
            this.guardarStorage();


            alert.present();

          });

      } else {

        // // Creo el loading
        this.loading = this.loadingCtrl.create({
          content: 'Cargando datos...',
        });
        // //muestro el loadigng
        this.loading.present();
        //obtengo los productos
        this.getStocks()
          .subscribe(data => {
            console.log(data);
            this.stocks = data;
            //cierro loading
            this.loading.dismiss();
            //envio la lista actualizada para almacenar
            this.guardarStorage();
          });

      }
    })

  }


  // Utilizo está función para traer los datos del servidor
  getStocks() {
    //obtengo el token
    this.authHeader = this.token.getToken();
    //asigno el token a la cabecera
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.authHeader
    });
    //realizo la peticíon
    return this.http.get(this.path, { headers });
  }



  // Sino tengo internet le indico a la app que use la
  // ultima lista de prouctos que descargamos.
  // Compruebo si estoy almacenando en compu o en celu
  cargarStorage() {

    let promesa = new Promise((resolve, reject) => {

      if (this.platform.is("cordova")) {

        console.log("inicializando storage mobile")
        this.storage.ready()
          .then(() => {

            this.storage.get("stocks")
              .then(stocks => {
                this.stocks = stocks;
                console.log("uso stock ya guardado mobile");
                resolve();
              })
          })
      } else {

        if (localStorage.getItem("stocks")) {

          this.stocks = JSON.parse(localStorage.getItem("stocks"));
          console.log("uso el que ya está guardado desktop");
          resolve();

        } else {
          console.log("no hay archivo guardado");
          this.primeraCarga();
          resolve();
        }

      }

    });
    return promesa;

  }

  // Guardo la lista de productos actualizada.
  guardarStorage() {

    if (this.platform.is("cordova")) {
      this.storage.ready()
        .then(() => {

          this.storage.set("stocks", this.stocks);
          console.log("se guardo el archivo de stocks en mobile.");

        })

    } else {
      // este codigo sirve para guardar en local desktop, pero
      // lo desactivo porque si guardo cliente y productos excedo
      // el espacio que me dan los browser para almacentar
      // localStorage.setItem("stocks", JSON.stringify(this.stocks) );
      // console.log("se guardo el archivo de stocks en desktop.");
    }
  }


  getStocksFiltro(searchTerm, productosPor) {

    // obtengo el valor buscado por el usuario
    const val = searchTerm;

    //obtengo el filtro agregado por el usuario
    const buscarProductosPor = productosPor;


    //Se filtra o por campo dni o por apellido
    if (buscarProductosPor == "nombre") {

      // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
        return this.stocks.filter((stocks) => {
          return (stocks.producto.descripcion.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      } else {
        console.log("salida1");
        return this.stocks;

      }

    } else if (buscarProductosPor == "codigo") {
      if (val && val.trim() != '') {
        return this.stocks.filter((stock) => {
          return (stock.producto.codigo.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      } else {
        console.log("salida2");
        return this.stocks;
      }

    } else {
      if (val && val.trim() != '') {
        this.stocks.filter((stock) => {
          return (stock.producto.marca.descripcion.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      } else {

        console.log("salida3");
        return this.stocks;
      }
    }
  }


}
