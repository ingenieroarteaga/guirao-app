export interface Marcas {
    id: number;
    descripcion: string;
    fechaBaja: string;
}