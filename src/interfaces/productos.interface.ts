export interface Productos {

  "id" : number,
  "modelo" : number,
  "descripcion" :string,
  "codigo" : number,
  "codBarra" : string,
  "precioCompra" : number,
  "ganancia" :number,
  "presentacion" : string,
  "fechaBaja" : string,
  "familiaProd" : {
    "id": number,
    "descripcion": string
  },
  "marca" : {
    "id" : number,
    "descripcion" : string,
    "fechaBaja" : string
  },
  "proveedor" : string,
  "rubro" : string,
  "tipoProducto" : string,
  "concepto" : {
    "id" : number,
    "descripcion" : string,
    "idFe" : number,
    "fechaBaja" : string
  },
  "iva" : {
    "id" : number,
    "valor" : number,
    "idFe" : number,
    "fechaBaja" : string
  }
}
