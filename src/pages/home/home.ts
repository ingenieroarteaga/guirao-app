import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {ClientesPage} from "../clientes/clientes";
import {ProductosPage} from "../productos/productos";
import {PedidosPage} from "../pedidos/pedidos";
import {AjustesPage} from "../ajustes/ajustes";
import {ResumenPage} from "../resumen/resumen";
import { Platform, LoadingController } from 'ionic-angular';

//Para alertas
import {AlertController} from "ionic-angular";
//Para login
import {AuthProvider}  from "../../providers/auth/auth";
import {TokenProvider}  from "../../providers/token/token";
//Cargar Clientes
import {ClientesProvider}  from "../../providers/clientes/clientes";
//Cargar Stock
import {StockProvider}  from "../../providers/stock/stock";
//Para almacenar información en el dispositivo
import { Storage } from '@ionic/storage/esm5';

import { Network } from '@ionic-native/network';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [AuthProvider, TokenProvider]

})
export class HomePage {

  username: string;
  password: string;
  loading: any;

  constructor(public navCtrl: NavController, private authService: AuthProvider,
              private token: TokenProvider, public clientesProvider: ClientesProvider,
              public network: Network, public stocksProvider: StockProvider,
              public platform: Platform, public loadingCtrl: LoadingController,
              public alertController: AlertController, public storage: Storage) {


    // compruebo si hay un username guardado
    // para pruebas en escirtorio usar localStorage.getItem
    // console.log(localStorage.getItem("username"));
    // if (localStorage.getItem("username")) {
    // console.log(storage.get("username"));
    // if (storage.get("username")) {
    //   this.obtengoUsuario().then((res) => {
    //   console.log(res)
    //   this.obtengoToken();
    // })

    storage.get("username").then((val)=> {
      console.log(val);
      if (val != undefined) {
        this.obtengoUsuario();
      } else {
        const alert =  this.alertController.create({
          message: 'DEBE ASIGNAR UN USUARIO',
          buttons: ['OK']
        });
        alert.present();
      }

    })

    // } else {
    //   const alert =  this.alertController.create({
    //     message: 'DEBE ASIGNAR UN USUARIO',
    //     buttons: ['OK']
    //   });
    //   alert.present();
    // }

  }

  obtengoUsuario() {
    let promesa = new Promise((resolve, reject)=> {
      // compruebo si es mobile o desktop
      if (this.platform.is("cordova")) {
        this.storage.ready()
                   .then(()=>{
                    console.log("cargo el username del storage");
                    this.storage.get("username")
                      .then( username=> {
                        this.username = username;
                        console.log("username cargado");
                        console.log("cargo la contraseña del storage");
                        this.storage.get("password")
                          .then( password=> {
                            this.password = password;
                            console.log("password cargado");
                            this.obtengoToken();
                          })
                      })
                  });
        resolve ();
      } else {
        this.username = JSON.parse(localStorage.getItem("username"));
        console.log("se cargo el username");
        this.password = JSON.parse(localStorage.getItem("password"));
        console.log("se cargo el password");
        this.obtengoToken();
        resolve ();
      }
    });
    return promesa;
  }

  obtengoToken() {
      console.log(this.username);
      console.log(this.password);
      this.authService.attemptAuth(this.username, this.password).subscribe(
        data => {

            this.token.saveToken(data.token);
            console.log("token OK");
            this.cargarDatos();
          },
        err => {
          const alert =  this.alertController.create({
            message: 'Usuario y/o contraseña incorrecta',
            buttons: ['OK']
          });
          alert.present();
        },
      );
  };

  cargarDatos() {

    //si tengo internet pido la lista de productos actualizada
    //sino tengo internet utilizo la ultima lista guardada
    // //en desktop isConnected no funcion, devuelve null siempre
    this.platform.ready().then(() => {

      if (this.isConnected()) {

         this.clientesProvider.primeraCarga();
         this.stocksProvider.primeraCarga();

      } else {
        // // Creo el loading
        this.loading =  this.loadingCtrl.create({
            content: 'Cargando datos locales...',
        });
        // //muestro el loadigng
        this.loading.present();

        this.stocksProvider.cargarStorage().then( ()=>{

            this.loading.dismiss();

          });
        this.clientesProvider.cargarStorage();
      }

    });


  }

  isConnected(): boolean {
    let conntype = this.network.type;
    return conntype && conntype !== 'unknown' && conntype !== 'none';
  }

  irPaginaProductos() {
    this.navCtrl.push(ProductosPage);
  }

  irPaginaClientes() {

    this.navCtrl.push(ClientesPage);
  }

  irPaginaPedidos() {

    this.navCtrl.push(PedidosPage);
  }

  irPaginaAjustes() {

    this.navCtrl.push(AjustesPage);
  }
  irPaginaResumen() {

    this.navCtrl.push(ResumenPage);
  }


}
