import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ResumenProvider} from '../../providers/resumen/resumen';
import {Pedido} from '../pedidos/pedido';

/**
 * Generated class for the ResumenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-resumen',
  templateUrl: 'resumen.html',
})
export class ResumenPage {

  public titulo: String;
  public pedidoVentas: any;
  public loading : boolean;
  p : number;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public resumenProvider: ResumenProvider) {

                this.resumenProvider.getPedidosVentas().subscribe(
                      data => {

                          this.pedidoVentas = data;
                          console.log('Pedidos cargados');
                        },
                      );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResumenPage');
  }

}
