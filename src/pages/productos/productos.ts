import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Searchbar, MenuController } from 'ionic-angular';

import { StockProvider } from "../../providers/stock/stock";
import { MarcasProvider } from '../../providers/marcas/marcas';
import { Marcas } from '../../interfaces/marcas.interface';
import { FamiliaProductosProvider } from '../../providers/familia-productos/familia-productos';
import { familiaProd } from '../../interfaces/familia-prod';


/**
 * Generated class for the ProductosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-productos',
  templateUrl: 'productos.html',
})


export class ProductosPage {

  @ViewChild('searchbar') searchbar: Searchbar;

  stocks: any = [];
  searchTerm: string = '';
  buscarProductosPor: string = 'codigo';
  type: string = 'number';
  marcas: any = [];
  familiaProd: any = [];

  // agregar o quitar elemento a los tags
  addMarcasLista: any[] = [];
  FamiliaProductoLista: any[] = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public stocksProvider: StockProvider,
    public menuCtrl: MenuController,
    public marcasProvider: MarcasProvider,
    public familiaProdProvicer: FamiliaProductosProvider
  ) {

  }

  async ionViewDidLoad() {
    this.setProductosFiltrados();
    this.marcasProvider.getMarcas().subscribe(marca => {
      // console.log('que traigo de la entidad marca: ', marca);
      this.marcas = marca;
    });
    this.familiaProdProvicer.getProductosFamilia().subscribe(familia => {
      this.familiaProd = familia;
      console.log('que trae las familias de productos:', this.familiaProd);
      
    });
  }



  ionViewDidEnter() {
    // pongo el foco en el input de busqueda para mostrar teclado
    setTimeout(() => {
      this.searchbar.setFocus();
      console.log('set focus active')
    }, 600);
  }

  setProductosFiltrados() {
    this.stocks = this.stocksProvider.getStocksFiltro(this.searchTerm, this.buscarProductosPor);
  }

  // Para cambiar el tipo de teclado
  tipoDeBusqueda() {

    if (this.buscarProductosPor !== 'codigo') {
      this.type = 'text';
      this.searchTerm = '';
      console.log('cambio a tipo texto');
      this.focusSearchbar();
    } else {
      this.type = 'number';
      this.searchTerm = '';
      console.log('cambio a tipo numerico');
      this.focusSearchbar();
    }
  };
  // Poner foco en searchbar y mostrar teclado
  focusSearchbar() {
    setTimeout(() => {
      this.searchbar.setFocus();
      console.log('set focus active')
    }, 1000);
  }

  // menu lateral
  openMenu() {
    this.menuCtrl.open();
  }

  // openMenu2() {
  //   this.menuCtrl.open();
  // }

  //  menuAdmin() {
  //   this.menuCtrl.enable(true, 'marca');
  //   this.menuCtrl.enable(true, 'familia');
  //  }


  //agregando marcas
  agregaMarca(marca: Marcas) {
    this.addMarcasLista.push(marca);
    console.log('agrego a la lista MARCA', this.addMarcasLista);
    this.getProductosByMarca(marca);
  }

  //quito marca
  quitarMarca(marca: Marcas, i: number) {
    this.addMarcasLista.splice(i, 1);
    console.log('quito de la lista MARCA', this.addMarcasLista);
    
  }

  //agregar familia
  agregaFamiliaProd(familia: familiaProd){
    this.FamiliaProductoLista.push(familia);
    console.log('agrego a la lista FAMILIAS', this.FamiliaProductoLista);
  }

  quitarFamiliaProd(familia: familiaProd, i: number) {
    this.FamiliaProductoLista.splice(i, 1);
    console.log('quito de la lista FAMILIAS', this.FamiliaProductoLista);
  }

  // traiga todo filtrado solo por lo que le paso en el arreglo
  getProductosByMarca(marca: any) {
    this.marcasProvider.filtrarMarcas(marca);
    console.log('que devuelve la busqueda por marcas:  ', this.marcasProvider.filtrarMarcas(marca));
    
  }

}
