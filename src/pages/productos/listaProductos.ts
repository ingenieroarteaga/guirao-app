import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Stock } from '../pedidos/stock';
import { Pedido } from '../pedidos/pedido';

export class ListaProductos {



    public id: number;
    public cantidad: number;
    public montoBonif: number;
    public porcBonif: number;
    public precioUnitario: number;
    public montoIva: number;
    public subTotal: number;
    public subTotalIva: number;
    public facturaDetalle: any;
    public pedidoEntrega: Pedido;
    public stock: Stock;

}
