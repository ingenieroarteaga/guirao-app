import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { Platform, LoadingController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
//Para login
import {AuthProvider}  from "../../providers/auth/auth";
import {TokenProvider}  from "../../providers/token/token";
//Para alertas
import {AlertController} from "ionic-angular";
//Cargar Clientes
import {ClientesProvider}  from "../../providers/clientes/clientes";
//Cargar Stock
import {StockProvider}  from "../../providers/stock/stock";
//Para almacenar información en el dispositivo
import { Storage } from '@ionic/storage/esm5';

/**
 * Generated class for the AjustesModalUsuarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ajustes-modal-usuario',
  templateUrl: 'ajustes-modal-usuario.html',
})
export class AjustesModalUsuarioPage {

  public username: string = '';
  public password: string = '';
  public userActivo: boolean = true;
  public usernameActivo: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public viewCtrl: ViewController, public alertas: AlertController,
              private authService: AuthProvider, private token: TokenProvider,
              public clientesProvider: ClientesProvider, public network: Network,
              public stocksProvider: StockProvider, public platform: Platform,
              public loadingCtrl: LoadingController, public storage: Storage,) {

    // compruebo si hay un username guardado, si hay muestro el nombre
    // para pruebas en escirtorio usar localStorage.getItem
    // console.log(localStorage.getItem("username"));
    // if (localStorage.getItem("username")) {
    this.storage.get("username").then((data)=>{
      if (data != undefined) {
         this.userActivo = false;
         // compruebo si es mobile o desktop
         if (this.platform.is("cordova")) {
           console.log("cargo el username del storage");
           this.storage.ready()
                     .then(()=>{
                       this.storage.get("username")
                         .then( username=> {
                           this.usernameActivo = username;
                           console.log("username cargado");
                         } )
                     });


         } else {
           this.usernameActivo = JSON.parse(localStorage.getItem("username"));
           console.log("se cargo el username");
         }
      }
    })
  }

  iniciaSesion() {

    if (this.username == '') {
        const alert =  this.alertas.create({
          message: 'Debe ingresar un nombre de usuario',
          buttons: ['OK']
        });
        alert.present();
    } else if (this.password =='') {
      const alert =  this.alertas.create({
        message: 'Debe ingresar una contraseña',
        buttons: ['OK']
      });
      alert.present();
    } else {
      this.authService.attemptAuth(this.username, this.password).subscribe(
        data => {
          this.token.saveToken(data.token);
          const alert =  this.alertas.create({
            message: 'El login se realizo de forma correcta',
            buttons: ['OK']
          });
          alert.present();
          this.usernameActivo = this.username;
          this.userActivo = false;
          this.guardarUsuario();
          this.cargarDatos();
          console.log("token OK");
        },
        err => {
          const alert =  this.alertas.create({
            message: 'Usuario y/o contraseña incorrecta',
            buttons: ['OK']
          });
          alert.present();
        },
      );
    }
  }


  // Guardo la lista de clientes actualizada.
  guardarUsuario() {

    if (this.platform.is("cordova")) {

      this.storage.ready()
                  .then(()=>{

                    this.storage.set("username", this.username);
                    console.log("se guardo el nombre de usuario.");
                    this.storage.set("password", this.password);
                    console.log("se guardo la contraseña.");

                  })

    } else {
      // este codigo sirve para guardar en local desktop
      localStorage.setItem("username", JSON.stringify(this.username) );
      console.log("se guardo el username.");
      localStorage.setItem("password", JSON.stringify(this.password) );
      console.log("se guardo el password.");
    }

  }

  cargarDatos() {
    //si tengo internet pido la lista de productos actualizada
    //sino tengo internet aviso que tengo que conectarme para obtener la info
    // //en desktop isConnected no funcion, devuelve null siempre
    this.platform.ready().then(() => {

      if (this.isConnected()) {

         this.clientesProvider.primeraCarga();
         this.stocksProvider.primeraCarga();

      } else {
        const alert =  this.alertas.create({
          message: 'Para descargar la información debe conectarse a internet',
          buttons: ['OK']
        });
        alert.present();
        // en desktop isConnected siempre da nulo, para hacer pruebas
        // llamo a la primera carga acá también
        // this.clientesProvider.primeraCarga();
        // this.stocksProvider.primeraCarga();
      }

    });


  }

  isConnected(): boolean {
    let conntype = this.network.type;
    return conntype && conntype !== 'unknown' && conntype !== 'none';
  }

  cerrarModal() {

    this.viewCtrl.dismiss( );
  }

}
