import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Searchbar } from 'ionic-angular';
import { StockProvider } from "../../providers/stock/stock";
import { AlertController } from 'ionic-angular';

import { ListaProductos } from '../productos/listaProductos';
import { Stock } from '../pedidos/stock';

/**
 * Generated class for the PedidosModalProductosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pedidos-modal-productos',
  templateUrl: 'pedidos-modal-productos.html',
})
export class PedidosModalProductosPage {

  @ViewChild('searchbar') searchbar: Searchbar;

  stocks: any = [];
  searchTerm: string = '';
  buscarProductosPor: string = 'codigo';
  type: string = 'number';
  // para carga de productos
  listaProducto: ListaProductos = new ListaProductos();
  listaProductos: ListaProductos[] = [];


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public stocksProvider: StockProvider, public viewCtrl: ViewController,
    public alertCtrl: AlertController) {



  }

  ionViewDidLoad() {

    //Llamo la primera vez para que se cargue mi lista de productos
    this.setProductosFiltrados();

  }
  ionViewDidEnter() {
    // pongo el foco en el input de busqueda para mostrar teclado
    setTimeout(() => {
      this.searchbar.setFocus();
      console.log('set focus active')
    }, 300);
  }

  //Envio datos para obtener lista completa o filtrada de productos
  setProductosFiltrados() {
    this.stocks = this.stocksProvider.getStocksFiltro(this.searchTerm, this.buscarProductosPor);
  }


  //cuando cierro el modal sin elegir producto
  cerrarModal() {

    // this.viewCtrl.dismiss( {stock: null , productoPrecio: null , cantidad: null ,} );
    this.viewCtrl.dismiss(this.listaProductos);

  }


  //cuando elijo un producto se muestra alerta para cargar cantidad
  showPrompt(stock) {
    const prompt = this.alertCtrl.create({
      title: 'Cantidad',
      inputs: [
        {
          name: 'unidad',
          type: 'number',
          placeholder: 'Unidades',
        },
        {
          name: 'cajas',
          type: 'number',
          placeholder: 'Cajas',
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
            this.searchTerm = '';
            this.focusSearchbar();
          }
        },
        {
          text: 'Cargar',
          handler: data => {
            console.log('Cargar clicked');
            console.log(data.unidad);
            if (data.unidad === '') {
              data.unidad = 1;
              console.log(data.unidad);
            }
            this.listaProducto.stock = new Stock();
            this.listaProducto.stock = stock;
            this.listaProducto.precioUnitario = stock.producto.precioCompra;
            this.listaProducto.cantidad = data.unidad;
            this.listaProducto.subTotal = stock.producto.precioCompra * data.unidad;;
            this.listaProducto.porcBonif = 0;
            this.listaProducto.montoBonif = 0;

            this.listaProductos.push(this.listaProducto);
            console.log(this.listaProducto);
            console.log(this.listaProductos);
            this.listaProducto = new ListaProductos();
            this.searchTerm = '';
            this.focusSearchbar();
          }
        }
      ]
    });
    prompt.present();
  }

  // Para cambiar el tipo de teclado
  tipoDeBusqueda() {

    if (this.buscarProductosPor !== 'codigo') {
      this.type = 'text';
      this.searchTerm = '';
      console.log('cambio a tipo texto');
      this.focusSearchbar();
    } else {
      this.type = 'number';
      this.searchTerm = '';
      console.log('cambio a tipo numerico');
      this.focusSearchbar();
    }
  };
  // Poner foco en searchbar y mostrar teclado

  focusSearchbar() {
    setTimeout(() => {
      this.searchbar.setFocus();
      console.log('set focus active')
    }, 1000);
  }



}
