import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// PARA USAR CON DATOS LOCALES
// import {CLIENTES} from "../../data/data.clientes";
// import {Clientes} from "../../interfaces/clientes.interface";

// //PARA USAR CON DATOS DE LA API
import { ClientesProvider } from "../../providers/clientes/clientes";


@Component({
  selector: 'page-clientes',
  templateUrl: 'clientes.html',
})
export class ClientesPage {


  clientes: any = [];
  searchTerm: string = '';
  buscarClientePor: string = 'nombre';



  constructor(public navCtrl: NavController, public navParams: NavParams,
    public clientesProvider: ClientesProvider) {
  }

  ionViewDidLoad() {
    this.setClientesFiltrados();
  }

  setClientesFiltrados() {
    this.clientes = this.clientesProvider.getClientesFiltro(this.searchTerm, this.buscarClientePor);
  }

  onChange(value) {
    this.buscarClientePor = value;
  }

}
