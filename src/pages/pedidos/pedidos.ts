import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import {PedidosModalClientePage}  from "../pedidos-modal-cliente/pedidos-modal-cliente";
import {PedidosModalProductosPage}  from "../pedidos-modal-productos/pedidos-modal-productos";
import { AlertController } from 'ionic-angular';
import { Network } from '@ionic-native/network';

import { Cliente } from '../clientes/cliente';
import { Producto } from '../productos/producto';
import { Stock } from '../pedidos/stock';
import { Sucursal } from '../pedidos/sucursal';
import { ListaProductos } from '../productos/listaProductos';
import { Pedido } from './pedido';

import {PedidosProvider} from "../../providers/pedidos/pedidos";
import {StockProvider} from "../../providers/stock/stock";

/**
 * Generated class for the PedidosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pedidos',
  templateUrl: 'pedidos.html',
})
export class PedidosPage {

  public pedido: Pedido = new Pedido();
  cliente: Cliente = new Cliente();
  producto: Producto = new Producto();
  sucursal: Sucursal = new Sucursal();
  // listaProducto: ListaProductos = new ListaProductos();
  listaProductos: ListaProductos[] = [];

  //para que aparezca el boton enviar.
  clienteCargado:boolean= false;
  productoCargado:boolean = false;



  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              public alertCtrl: AlertController,
              private pedidosProvider: PedidosProvider,
              public network: Network,
              public stockProvider: StockProvider) {
              //
              // this.cargadorInicial();
              // this.providerStock.getStocks();
  }

  // cargadorInicial() {
  //
  //   this.cliente.nombre.push('gonza');
  //   this.productoCodigo.push('arteaga');
  //   this.productoPrecio.push('32002302');
  // }

  //Abro el modal para elegir cliente
  openModalCliente() {

    let chooseModalCliente = this.modalCtrl.create(PedidosModalClientePage);
      chooseModalCliente.onDidDismiss(data => {
        // this.clienteNombre  = data['cliente'];
        // this.clienteApellido = data['clienteApellido'];
        // this.clienteCuit = data['clienteCuit'];
        this.cliente.nombre  = data['cliente'];
        this.cliente.apellido = data['clienteApellido'];
        this.cliente.cuit = data['clienteCuit'];
        this.cliente.id = data['clienteId'];
        this.clienteCargado =  true;

    });
    chooseModalCliente.present();

  }

  //abro el modal para elgir productos
  openModalProductos() {

    let chooseModalProductos = this.modalCtrl.create(PedidosModalProductosPage);
    chooseModalProductos.onDidDismiss(data => {

      //Compruebo si cerraron el modal con la x, de lo contrario
      //agrego el producto elegido a la lista
      if ( data.length !== 0) {
        // Carga la lista de productos a la lista local
        data.forEach(element => {
          this.listaProductos.push(element);
        });

        console.log(data);
        console.log(this.listaProductos);
        //Actualizo el monto total de la lista
        this.calcularTotales();
        //cambio a true el estado de producto cargado para mostrar el boton Enviar
        this.productoCargado =  true;

      } else {
        console.log('el modal se cerro sin cargar productos');
      }

    });
    chooseModalProductos.present();

  }

  //Para calcular los subtotales de cada prodcuto
  calcularSubtotales(p_pedidoDetalle){
    if(p_pedidoDetalle.precioUnitario !== null && p_pedidoDetalle.cantidad !== null){
      p_pedidoDetalle.subTotal = p_pedidoDetalle.precioUnitario * p_pedidoDetalle.cantidad;
      this.calcularTotales();
      // this.redondear();
    } else {
      p_pedidoDetalle.subTotal = 0;
      this.calcularTotales();
    }
    // if(typeof p_pedidoDetalle.porcBonif !== 'undefined' && p_pedidoDetalle.porcBonif !== null){
    //   p_pedidoDetalle.montoBonif = p_pedidoDetalle.porcBonif / 100 * p_pedidoDetalle.subTotal;
    //   p_pedidoDetalle.subTotal = p_pedidoDetalle.subTotal - p_pedidoDetalle.montoBonif;
    // } else {
    //   p_pedidoDetalle.montoBonif = 0;
    // }
    // if(p_pedidoDetalle.stock.producto.iva.valor !== null){
    //   p_pedidoDetalle.montoIva = p_pedidoDetalle.stock.producto.iva.valor / 100 * p_pedidoDetalle.subTotal;
    //   p_pedidoDetalle.subTotalIva = p_pedidoDetalle.subTotal + p_pedidoDetalle.montoIva;
    // } else {
    //   p_pedidoDetalle.montoIva = 0;
    //   p_pedidoDetalle.subTotalIva = p_pedidoDetalle.subTotal;
    // }
  }

  //calculo la suma de subtotales de todos los productos
  calcularTotales(){
    this.pedido.monto = 0;
    this.listaProductos.forEach(element => {
        this.pedido.monto = this.pedido.monto + element.subTotal;
        console.log(this.pedido.monto);
    });
  }


  //Para borrar producto de la lista de productos a enviar
  removeProducto(index){

    this.listaProductos.splice(index, 1);

  }

  //Una vez cargado el producto si hay conexión se envia sino se
  //almacena y espera la proxima conexón
  enviar() {

      if (this.isConnected()) {

        if(typeof this.listaProductos !== 'undefined' && this.listaProductos !== null && this.listaProductos.length > 0){
          this.pedido.pedidoEntregaDetalleList = this.listaProductos;
          this.pedido.cliente = this.cliente;
          this.pedido.numero = 0;
          this.pedido.fecha =  new Date() ;
          this.pedido.fechaEntrega =  new Date();
          this.pedido.descripcion = 'Pedido desde la app';
          this.pedido.descuento= 0;
          //cuando usamos servidor local el id de sucursl es 7
          this.sucursal.id = 1;
          this.sucursal.descripcion = "Casa Central";
          this.pedido.sucursal = this.sucursal;

          if (typeof this.pedido.id === 'undefined' || this.pedido.id === null) {
            this.pedidosProvider.create(this.pedido);
            console.log(this.pedido);
          } else {
              this.pedidosProvider.update(this.pedido).subscribe(pedidoVenta => {

              });
          }

        }
        const alert = this.alertCtrl.create({
          title: 'Pedido Enviado',
          subTitle: 'Tu pedido ya fue enviado al deposito',
          buttons: [{
            text: 'ok',
            handler: data => {
              this.navCtrl.popToRoot()
            }
          },]

        });

        alert.present().then();


      } else {

        const alert = this.alertCtrl.create({
          title: 'Pedido no enviado',
          subTitle: 'No hay conexión a internet',
          buttons: [{
            text: 'ok',
            handler: data => {
              this.navCtrl.popToRoot()
            }
          },]

        });

        alert.present().then();

      }


  }

  isConnected(): boolean {
    let conntype = this.network.type;
    console.log(conntype);
    return conntype && conntype !== 'unknown' && conntype !== 'none';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PedidosPage');
  }
  ViewWillLeave() {
    console.log('se ejectuto ionViewWillLeave');
  }
  ViewDidLeave() {
    console.log('se ejectuto ionViewDidLeave');
  }
  ViewWillUnload() {
    console.log('se ejectuto ionViewWillUnload');
  }

  // ionViewWillLeave() {
  //
  //   console.log('ionViewDidLoad PedidosPage');
  //   return new Promise((resolve, reject) => {
  //       this.alertCtrl.create({
  //           enableBackdropDismiss: false,
  //           title: '¿Salir de pedido?',
  //           message: 'Si confirma perdera el pedido',
  //           buttons: [{
  //               text: "Salir",
  //               handler: resolve
  //           },{
  //               text: "Cancelar",
  //               handler: reject
  //           }]
  //       }).present();
  //   });
  // }
}
