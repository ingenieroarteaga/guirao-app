import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PedidosModalClientePage } from './pedidos-modal-cliente';

@NgModule({
  declarations: [
    PedidosModalClientePage,
  ],
  imports: [
    IonicPageModule.forChild(PedidosModalClientePage),
  ],
})
export class PedidosModalClientePageModule {}
