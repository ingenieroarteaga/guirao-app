import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController,LoadingController,Searchbar } from 'ionic-angular';

// PARA USAR CON DATOS LOCALES
//import {CLIENTES} from "../../data/data.clientes";
//import {Clientes} from "../../interfaces/clientes.interface";

// //PARA USAR CON DATOS DE LA API
import {ClientesProvider} from "../../providers/clientes/clientes";


@Component({
  selector: 'page-pedidos-modal-cliente',
  templateUrl: 'pedidos-modal-cliente.html',
})
export class PedidosModalClientePage {

  @ViewChild('searchbar') searchbar:Searchbar;

  clientes: any = [];
  buscarClientePor: string ='nombre';
  searchTerm: string ='';


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public clientesProvider: ClientesProvider, ) {
  }


  ionViewDidLoad() {
      this.setClientesFiltrados();
  }
  ionViewDidEnter() {
      // pongo el foco en el input de busqueda para mostrar teclado
      setTimeout(() => {
        this.searchbar.setFocus();
        console.log('set focus active')
   });
  }
  setClientesFiltrados() {
     this.clientes = this.clientesProvider.getClientesFiltro( this.searchTerm , this.buscarClientePor);
  }

  onChange(value) {
    this.buscarClientePor = value;
  }

  cerrarModal(clienteNombre, clienteApellido, clienteCuit, clienteId) {
    console.log(clienteNombre, clienteApellido, clienteCuit, clienteId);
    this.viewCtrl.dismiss( {cliente: clienteNombre, clienteApellido: clienteApellido, clienteCuit: clienteCuit, clienteId: clienteId } );

  }

}
